# Summary Report for PyCon MY 2018

### Abstract

This summary report gives an overview on what was happening on PyCon My 2018.

### PyCon MY 2018

PyCon MY 2018 was held on 25 and 26 August 2018, together with the sprint, 27 August 2018 at Faculty of Computer Science and Information Technology, University of Malaya. The event had about 120 of participants, who came from the different region of the world.

There were 2 different sessions running concurrently, withouth differentiating the track. In total, there were 2 sessions of keynote, together with 27 sessions of different talk, which comprised of Python programming related, Data Science/Machine Learning/Deep Learning, and application of Python programming.

We had a smaller organizing committee for PyCon MY 2018 compared to PyCon MY 2016, which consisted of 4 people, assisted by 20 volunteers, led by Associate Prof. Dr. Chiew Thiam Kian from University of Malaya.

### Speakers

To avoid any bias in selecting the speakers during the Call-For-Paper (CFP) session, whether in terms of person or the field of the submission, we had a review team to review all the submissions, ensuring that the conference had a variety of applications in Python programming, as stated in the CFP landing page. The admission rate for PyCon MY 2018 was about 50%, which was a result of the reviewers' prudent effort, ascertaining that we covered most of the aspects in Python programming. After a thorough revision, we had 33.3 % from Data Science/Machine Learning/Deep Learning, 22.2% of Python programing projects, and 44.5 % of other Python related projects such as Blockchain application, IOT projects, how Python can be used in SEA traditional music instrumental analysis and etc.

Apart from ascertaining the diversity in selecting the range of the topic, We also made sure there is a diversity in both gender and region. Most of speakers were local speakers, which consisted of 36%, and the second most countries were both India and Indonesia, which both comprised of 17%. The remaining speakers are coming from China, Japan, Philipines, Singapore, South Korea and Thailand. Although the portion of female speakers in the speakers' pool is small, which is 14.3%, but compared to PyCon MY 2016, there was a significant improvement in the female speakers' portion, as a result of promoting female to speak in the conference through different channels.

![speakers](speakers.png)  
Figure: Distributions of Speakers in PyCon MY 2018

To further promoting the collaboration and civil involvement using technology, we had Khairil Yusof and Choong Yue Lin as our keynote speakers. During the keynotes, both speakers demonstrated how Python programming could be used a channel to promote a civil issue/project so that different people can contribute themself in different roles of the issue or projects. The speakers used various examples in their project to convince the participants that it does not take a big effort in get involved in the civil project, rather through a series of small projects.

### Participants

Compared to PyCon MY 2016, PyCon MY 2018 had 123 participants this year, with an increment of 33%. Out of all of the participants, most of them were first time attending PyCon MY 2018, which consisted of 69%, meanwhile 31% are regular participants.

![participants](participants.png)  
Figure: Distribution of the participants for PyCon MY 2018.

#### Distributions in terms of professionals

Out of the 123 participants, 93 of the participants are working professionals in different industries, ranging from Start-Up, to Corporate and GLCs, 20 of them are both students and working professionals. Full time students take up 16.33% of the participants, and 2.04 % of the participants do not belong to any group mentioned above.

![students_working](students_working.png)  
Figure: Distribution of different professionals among participants coming to PyCon MY 2018.

#### Gender Diversity

Compared to 2016, We had more female participants in PyCon MY 2018. Out of 123 participants, there were 21 female participants this year, while there were only less than 5 female participants in PyCon MY 2016. Eventhough the numbers of female participants were still minuscule compared to other conferences, but we managed to attract more female participants through various channels and helps from other communities such as Women Who Code Kuala Lumpur (WWCKL). This is also our initiative in promoting Python to the female programming communities. One of WWCKL directors was also in the organizing team of PyCon MY 2018.

![gender](male_female.png)  
Figure: Gender distribution among particpants coming to PyCon MY 2018.

#### Python Usage Experience

According to our survey, 57.73% of our participants have less than 1 year of experience in Python. Those who have experience range from 1 to 5 years are 45.18% of total participants, meanwhile 20.08% are those with more than 5 years experience.

![experience](experience.png)  
Figure: Distributions of user's experience in Python.

### Reasons for Participants to Attend PyCon MY 2018

Based on the data collected from our survey, web services, data science/analytics and artificial intelligence related topics were main reasons for the participants to attend PyCon MY 2018. Due to the popularity of Python's application in data science and artificial intelligence, it is not a surprise to see this in PyCon MY 2018, but also in other PyCons.

Apart from the main stream applications in Python, the participants also expected to get latest insights on scientific applications, operations and administration of services, and community building in this PyCon, which takes 16%, 7% and 8% respectively.

![reason_to_participate](reason_to_participate.png)  
Figure: Reasons for Participants to join PyCon MY 2018.  

### Development Sprint

Development Sprint has been a tradition in more mature PyCon events held all over the world. However this was the second development sprint organized by PyCon MY. The session was held at Makmal Mikro 1 (Micro Laboratory 1) in the Faculty of Computer Science and Information Technology, University of Malaya, on Monday 27 August 2018. Steve Clarke, the Group Chief Technical Consultant of Ezy Mobile Sdn Bhd led the session, together with the assistance from Cindy Ooi, a Data Scientist from SEEK Asia.

7 people attended the session although 20 people registered initially. The main reasons given by absentees were last minute work commitment and health issue.

Out of the 7 participants, 4 participants had attended the PyCon MY 2018 conference day held two days earlier on 25 and 26 August 2018. In term of participants background, there were 4 postgraduate students while the other 3 are working professionals. All participants comes from various software development background except one participant has a background in accountancy.

The session started with a presentation to introduce the concept of development sprint to the participants and setting the expectation. Considering the fact that the participants are still new to Python language with no prior experience in participating in the development sprint, the session was conducted in a form of workshop.

Selected data science problems from Kaggle challenge were used in case studies, as well as practical example. In the afternoon, the participants were led to work on an open source project, Scrapy. Although there were no pull requests made, the session proved to be useful for the participants to understand how development sprint works, and a meaningful lesson for the organizer to consider for other session in future.
